Slickmenu Module
-------------------------------------

Slickmenu module will return horizontal menu block as a slider.

The module is dependent on drupal modules libraries and jQuery Update.
Make sure you have installed both the modules.

For this module you need to download slick js and to libraries.
Download Slick js with version 1.5.9 from
http://kenwheeler.github.io/slick/ and extract.
Inside the folder there will be folder named slick copy the folder a
nd place it in libraries directory.

Example
-------

For adding a simple slick menu block follow below steps:

- First a simple block.
- Next, create an array in following format with link title and path:
  $menu_items = array(
    0 => array('link_title' => 'Menu link 1', 'link_path' => 'path/demo1'),
    1 => array('link_title' => 'Menu link 2', 'link_path' => 'path/demo2'),
    2 => array('link_title' => 'Menu link 3', 'link_path' => 'path/demo3'),
    3 => array('link_title' => 'Menu link 4', 'link_path' => 'path/demo4'),
    4 => array('link_title' => 'Menu link 5', 'link_path' => 'path/demo5'),
    5 => array('link_title' => 'Menu link 6', 'link_path' => 'path/demo6'),
  );
- Pass this array to the theme 'slickmenu_items'
  $slickmenu_items[] = array(
    '#theme' => 'slick_menu_items',
    '#menu_items' => $menu_items,
  );
- You can render the '$slickmenu_items' for display.

This modules also provides another option for adding slick slider using
 admin configurations for that go to link admin/config/slickmenu,
 from there you could select the menu item desired from the listbox
 and click save.
This will output a block with slickmenu slider that can be placed in
 any regions of your choice.
