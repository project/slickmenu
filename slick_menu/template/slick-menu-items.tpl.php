<?php
/**
 * @file
 * Display for slickmenu block slider.
 */
?>

<div class="slider">
  <ul class="multiple-items">
  <?php foreach ($menu_items as $item): ?>
    <li>
      <a href="<?php print $item['link_path']; ?>"><?php print $item['link_title']; ?></a>
    </li>
  <?php endforeach; ?>
  </ul>
</div>
