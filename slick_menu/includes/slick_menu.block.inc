<?php
/**
 * @file
 * Contains custom page callbacks for slickmenu module.
 */

/**
 * This function will return the slickmenu block.
 *
 * @param array $menu_item
 *   Menu name.
 *
 * @return array $slickmenu_items
 *   Returns items for the slick block.
 */
function slick_menu_slickitems($menu_item = '') {
  $menu_links = menu_tree_all_data($menu_item);

  $menu_items = array();
  $menu_items = slick_menu_menuitems($menu_links);

  $slickmenu_items[] = array(
    '#theme' => 'slick_menu_items',
    '#menu_items' => $menu_items,
  );

  return $slickmenu_items;
}

/**
 * Function to fetch slickmenu links of the corresponding menu item.
 *
 * @param array $menu_links
 *   Menu links array.
 *
 * @return as $menu_items
 *   Returns processed menu array.
 */
function slick_menu_menuitems($menu_links) {
  $menu_items = array();
  foreach ($menu_links as $key => $links) {
    if (!empty($links['link']['link_title']) && $links['link']['hidden'] == SLICK_MENU_ENABLED) {
      $menu_items[$key]['link_title'] = $links['link']['link_title'];
      $menu_items[$key]['link_path'] = url($links['link']['link_path']);
    }
  }
  return $menu_items;
}
