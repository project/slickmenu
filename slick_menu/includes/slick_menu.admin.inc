<?php
/**
 * @file
 * Contains custom page callbacks for slickmenu module admin config.
 */

/**
 * This function will return the slickmenu config form.
 */
function slick_menu_config_form() {
  $menu_list_items = menu_get_menus();
  $form['slick_menu_item'] = array(
    '#type' => 'select',
    '#title' => t('Select menu for setting slickable menu.'),
    '#default_value' => variable_get('slick_menu_item', ''),
    '#options' => $menu_list_items,
    '#size' => 5,
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
