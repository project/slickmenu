/**
 * @file
 * Contains configuration settings for slickmenu block.
 */

var $ = jQuery;
$(document).ready(function() {
  $('.multiple-items').slick({
    arrows: Drupal.settings.slickmenu_style.arrows,
    infinite: Drupal.settings.slickmenu_style.infinite,
    speed: Drupal.settings.slickmenu_style.speed,
    slidesToShow: Drupal.settings.slickmenu_style.slidesToShow,
    slidesToScroll: Drupal.settings.slickmenu_style.slidesToScroll,
    variableWidth: Drupal.settings.slickmenu_style.variableWidth,
  });
});
